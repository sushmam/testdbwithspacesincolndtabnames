/*Copyright (c) 2015-2016 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/

package com.testdbwithspacesincolndtabnames.northwind;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * Orders generated by hbm2java
 */
@Entity
@Table(name="`Orders`"
    ,schema="NorthWind"
)
public class Orders  implements java.io.Serializable
 {


private Integer orderId;


@Type(type="DateTime")
private LocalDateTime orderDate;


@Type(type="DateTime")
private LocalDateTime requiredDate;


@Type(type="DateTime")
private LocalDateTime shippedDate;
private BigDecimal freight;
private String shipName;
private String shipAddress;
private String shipCity;
private String shipRegion;
private String shipPostalCode;
private String shipCountry;
private Customers customers;
private Employees employees;
private Shippers shippers;
private Set<OrderDetails> orderDetailses = new HashSet<OrderDetails>(0);

    public Orders() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="`OrderID`", nullable=false)
    public Integer getOrderId() {
        return this.orderId;
    }
    
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    

    @Column(name="`OrderDate`")
    public LocalDateTime getOrderDate() {
        return this.orderDate;
    }
    
    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    

    @Column(name="`RequiredDate`")
    public LocalDateTime getRequiredDate() {
        return this.requiredDate;
    }
    
    public void setRequiredDate(LocalDateTime requiredDate) {
        this.requiredDate = requiredDate;
    }

    

    @Column(name="`ShippedDate`")
    public LocalDateTime getShippedDate() {
        return this.shippedDate;
    }
    
    public void setShippedDate(LocalDateTime shippedDate) {
        this.shippedDate = shippedDate;
    }

    

    @Column(name="`Freight`")
    public BigDecimal getFreight() {
        return this.freight;
    }
    
    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    

    @Column(name="`ShipName`")
    public String getShipName() {
        return this.shipName;
    }
    
    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    

    @Column(name="`ShipAddress`")
    public String getShipAddress() {
        return this.shipAddress;
    }
    
    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    

    @Column(name="`ShipCity`")
    public String getShipCity() {
        return this.shipCity;
    }
    
    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    

    @Column(name="`ShipRegion`")
    public String getShipRegion() {
        return this.shipRegion;
    }
    
    public void setShipRegion(String shipRegion) {
        this.shipRegion = shipRegion;
    }

    

    @Column(name="`ShipPostalCode`")
    public String getShipPostalCode() {
        return this.shipPostalCode;
    }
    
    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    

    @Column(name="`ShipCountry`")
    public String getShipCountry() {
        return this.shipCountry;
    }
    
    public void setShipCountry(String shipCountry) {
        this.shipCountry = shipCountry;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`CustomerID`", nullable=false)
    public Customers getCustomers() {
        return this.customers;
    }
    
    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`EmployeeID`", nullable=false)
    public Employees getEmployees() {
        return this.employees;
    }
    
    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`ShipVia`", nullable=false)
    public Shippers getShippers() {
        return this.shippers;
    }
    
    public void setShippers(Shippers shippers) {
        this.shippers = shippers;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="orders")
    public Set<OrderDetails> getOrderDetailses() {
        return this.orderDetailses;
    }
    
    public void setOrderDetailses(Set<OrderDetails> orderDetailses) {
        this.orderDetailses = orderDetailses;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof Orders) )
		 return false;

		 Orders that = ( Orders ) o;

		 return ( (this.getOrderId()==that.getOrderId()) || ( this.getOrderId()!=null && that.getOrderId()!=null && this.getOrderId().equals(that.getOrderId()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getOrderId() == null ? 0 : this.getOrderId().hashCode() );

         return result;
     }


}


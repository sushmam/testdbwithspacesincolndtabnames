/*Copyright (c) 2015-2016 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/

package com.testdbwithspacesincolndtabnames.northwind.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.testdbwithspacesincolndtabnames.northwind.service.ProductsService;
import com.testdbwithspacesincolndtabnames.northwind.service.SuppliersService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.testdbwithspacesincolndtabnames.northwind.*;
import com.testdbwithspacesincolndtabnames.northwind.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class Suppliers.
 * @see com.testdbwithspacesincolndtabnames.northwind.Suppliers
 */
@RestController(value = "NorthWind.SuppliersController")
@RequestMapping("/NorthWind/Suppliers")
@Api(description = "Exposes APIs to work with Suppliers resource.", value = "SuppliersController")
public class SuppliersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SuppliersController.class);

    @Autowired
    @Qualifier("NorthWind.SuppliersService")
    private SuppliersService suppliersService;

    @Autowired
    @Qualifier("NorthWind.ProductsService")
    private ProductsService productsService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of Suppliers instances matching the search criteria.")
    public Page<Suppliers> findSupplierss(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Supplierss list");
        return suppliersService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of Suppliers instances.")
    public Page<Suppliers> getSupplierss(Pageable pageable) {
        LOGGER.debug("Rendering Supplierss list");
        return suppliersService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the Suppliers instance associated with the given id.")
    public Suppliers getSuppliers(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Suppliers with id: {}", id);
        Suppliers instance = suppliersService.findById(id);
        LOGGER.debug("Suppliers details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the Suppliers instance associated with the given id.")
    public boolean deleteSuppliers(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Suppliers with id: {}", id);
        Suppliers deleted = suppliersService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the Suppliers instance associated with the given id.")
    public Suppliers editSuppliers(@PathVariable("id") Integer id, @RequestBody Suppliers instance) throws EntityNotFoundException {
        LOGGER.debug("Editing Suppliers with id: {}", instance.getSupplierId());
        instance.setSupplierId(id);
        instance = suppliersService.update(instance);
        LOGGER.debug("Suppliers details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/productses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the productses instance associated with the given id.")
    public Page<Products> findAssociatedproductses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated productses");
        return productsService.findAssociatedValues(id, "suppliers", "supplierId", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new Suppliers instance.")
    public Suppliers createSuppliers(@RequestBody Suppliers instance) {
        LOGGER.debug("Create Suppliers with information: {}", instance);
        instance = suppliersService.create(instance);
        LOGGER.debug("Created Suppliers with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setSuppliersService(SuppliersService service) {
        this.suppliersService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of Suppliers instances.")
    public Long countAllSupplierss() {
        LOGGER.debug("counting Supplierss");
        Long count = suppliersService.countAll();
        return count;
    }
}

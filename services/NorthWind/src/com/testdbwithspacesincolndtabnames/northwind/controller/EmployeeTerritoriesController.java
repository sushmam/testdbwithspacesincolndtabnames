/*Copyright (c) 2015-2016 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/

package com.testdbwithspacesincolndtabnames.northwind.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.testdbwithspacesincolndtabnames.northwind.service.EmployeeTerritoriesService;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.testdbwithspacesincolndtabnames.northwind.*;
import com.testdbwithspacesincolndtabnames.northwind.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class EmployeeTerritories.
 * @see com.testdbwithspacesincolndtabnames.northwind.EmployeeTerritories
 */
@RestController(value = "NorthWind.EmployeeTerritoriesController")
@RequestMapping("/NorthWind/EmployeeTerritories")
@Api(description = "Exposes APIs to work with EmployeeTerritories resource.", value = "EmployeeTerritoriesController")
public class EmployeeTerritoriesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeTerritoriesController.class);

    @Autowired
    @Qualifier("NorthWind.EmployeeTerritoriesService")
    private EmployeeTerritoriesService employeeTerritoriesService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of EmployeeTerritories instances matching the search criteria.")
    public Page<EmployeeTerritories> findEmployeeTerritoriess(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering EmployeeTerritoriess list");
        return employeeTerritoriesService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of EmployeeTerritories instances.")
    public Page<EmployeeTerritories> getEmployeeTerritoriess(Pageable pageable) {
        LOGGER.debug("Rendering EmployeeTerritoriess list");
        return employeeTerritoriesService.findAll(pageable);
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the EmployeeTerritories instance associated with the given composite-id.")
    public EmployeeTerritories getEmployeeTerritories(@RequestParam("employeeId") Integer employeeId, @RequestParam("territoryId") String territoryId) throws EntityNotFoundException {
        EmployeeTerritoriesId temp = new EmployeeTerritoriesId();
        temp.setEmployeeId(employeeId);
        temp.setTerritoryId(territoryId);
        LOGGER.debug("Getting EmployeeTerritories with id: {}", temp);
        EmployeeTerritories instance = employeeTerritoriesService.findById(temp);
        LOGGER.debug("EmployeeTerritories details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the EmployeeTerritories instance associated with the given composite-id.")
    public boolean deleteEmployeeTerritories(@RequestParam("employeeId") Integer employeeId, @RequestParam("territoryId") String territoryId) throws EntityNotFoundException {
        EmployeeTerritoriesId temp = new EmployeeTerritoriesId();
        temp.setEmployeeId(employeeId);
        temp.setTerritoryId(territoryId);
        LOGGER.debug("Deleting EmployeeTerritories with id: {}", temp);
        EmployeeTerritories deleted = employeeTerritoriesService.delete(temp);
        return deleted != null;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the EmployeeTerritories instance associated with the given composite-id.")
    public EmployeeTerritories editEmployeeTerritories(@RequestParam("employeeId") Integer employeeId, @RequestParam("territoryId") String territoryId, @RequestBody EmployeeTerritories instance) throws EntityNotFoundException {
        EmployeeTerritoriesId temp = new EmployeeTerritoriesId();
        temp.setEmployeeId(employeeId);
        temp.setTerritoryId(territoryId);
        employeeTerritoriesService.delete(temp);
        instance = employeeTerritoriesService.create(instance);
        LOGGER.debug("EmployeeTerritories details with id is updated: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new EmployeeTerritories instance.")
    public EmployeeTerritories createEmployeeTerritories(@RequestBody EmployeeTerritories instance) {
        LOGGER.debug("Create EmployeeTerritories with information: {}", instance);
        instance = employeeTerritoriesService.create(instance);
        LOGGER.debug("Created EmployeeTerritories with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setEmployeeTerritoriesService(EmployeeTerritoriesService service) {
        this.employeeTerritoriesService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of EmployeeTerritories instances.")
    public Long countAllEmployeeTerritoriess() {
        LOGGER.debug("counting EmployeeTerritoriess");
        Long count = employeeTerritoriesService.countAll();
        return count;
    }
}

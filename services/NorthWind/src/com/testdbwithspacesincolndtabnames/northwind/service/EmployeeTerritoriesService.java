/*Copyright (c) 2015-2016 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/

package com.testdbwithspacesincolndtabnames.northwind.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.testdbwithspacesincolndtabnames.northwind.*;

/**
 * Service object for domain model class EmployeeTerritories.
 * @see com.testdbwithspacesincolndtabnames.northwind.EmployeeTerritories
 */

public interface EmployeeTerritoriesService {
   /**
	 * Creates a new employeeterritories.
	 * 
	 * @param created
	 *            The information of the created employeeterritories.
	 * @return The created employeeterritories.
	 */
	public EmployeeTerritories create(EmployeeTerritories created);

	/**
	 * Deletes a employeeterritories.
	 * 
	 * @param employeeterritoriesId
	 *            The id of the deleted employeeterritories.
	 * @return The deleted employeeterritories.
	 * @throws EntityNotFoundException
	 *             if no employeeterritories is found with the given id.
	 */
	public EmployeeTerritories delete(EmployeeTerritoriesId employeeterritoriesId) throws EntityNotFoundException;

	/**
	 * Finds all employeeterritoriess.
	 * 
	 * @return A list of employeeterritoriess.
	 */
	public Page<EmployeeTerritories> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<EmployeeTerritories> findAll(Pageable pageable);
	
	/**
	 * Finds employeeterritories by id.
	 * 
	 * @param id
	 *            The id of the wanted employeeterritories.
	 * @return The found employeeterritories. If no employeeterritories is found, this method returns
	 *         null.
	 */
	public EmployeeTerritories findById(EmployeeTerritoriesId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a employeeterritories.
	 * 
	 * @param updated
	 *            The information of the updated employeeterritories.
	 * @return The updated employeeterritories.
	 * @throws EntityNotFoundException
	 *             if no employeeterritories is found with given id.
	 */
	public EmployeeTerritories update(EmployeeTerritories updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the employeeterritoriess in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the employeeterritories.
	 */

	public long countAll();


    public Page<EmployeeTerritories> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}


/*Copyright (c) 2015-2016 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/

package com.testdbwithspacesincolndtabnames.northwind.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.testdbwithspacesincolndtabnames.northwind.*;


/**
 * ServiceImpl object for domain model class OrderDetails.
 * @see com.testdbwithspacesincolndtabnames.northwind.OrderDetails
 */
@Service("NorthWind.OrderDetailsService")
public class OrderDetailsServiceImpl implements OrderDetailsService {


    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDetailsServiceImpl.class);

    @Autowired
    @Qualifier("NorthWind.OrderDetailsDao")
    private WMGenericDao<OrderDetails, OrderDetailsId> wmGenericDao;
    public void setWMGenericDao(WMGenericDao<OrderDetails, OrderDetailsId> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "NorthWindTransactionManager")
     public Page<OrderDetails> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "NorthWindTransactionManager")
    @Override
    public OrderDetails create(OrderDetails orderdetails) {
        LOGGER.debug("Creating a new orderdetails with information: {}" , orderdetails);
        return this.wmGenericDao.create(orderdetails);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "NorthWindTransactionManager")
    @Override
    public OrderDetails delete(OrderDetailsId orderdetailsId) throws EntityNotFoundException {
        LOGGER.debug("Deleting orderdetails with id: {}" , orderdetailsId);
        OrderDetails deleted = this.wmGenericDao.findById(orderdetailsId);
        if (deleted == null) {
            LOGGER.debug("No orderdetails found with id: {}" , orderdetailsId);
            throw new EntityNotFoundException(String.valueOf(orderdetailsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "NorthWindTransactionManager")
    @Override
    public Page<OrderDetails> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all orderdetailss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "NorthWindTransactionManager")
    @Override
    public Page<OrderDetails> findAll(Pageable pageable) {
        LOGGER.debug("Finding all orderdetailss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "NorthWindTransactionManager")
    @Override
    public OrderDetails findById(OrderDetailsId id) throws EntityNotFoundException {
        LOGGER.debug("Finding orderdetails by id: {}" , id);
        OrderDetails orderdetails=this.wmGenericDao.findById(id);
        if(orderdetails==null){
            LOGGER.debug("No orderdetails found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return orderdetails;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "NorthWindTransactionManager")
    @Override
    public OrderDetails update(OrderDetails updated) throws EntityNotFoundException {
        LOGGER.debug("Updating orderdetails with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((OrderDetailsId)updated.getId());
    }

    @Transactional(readOnly = true, value = "NorthWindTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


